# SQL(Structured Query Language)
### What is SQL?
- SQL stands for Structured Query Language.
- It is designed for managing data in a relational database management system (RDBMS).
- SQL is a database language, it is used for database creation, deletion, fetching rows, and modifying rows, etc.
- All DBMS like MySQL, Oracle, Sybase, PostgreSQL, and SQL Server use SQL as standard database language.

### Why SQL is required?
- To create new databases, tables, and views.
- To insert records in a database.
- To update records in a database.
- To delete records from a database.
- To retrieve data from a database.
 
## SQL Basic Commands:

###### 1) SELECT: 
It will extract the data from a database.
```Syntax for SELECT```
```
SELECT "column_name" 
FROM "table_name"; 
```

###### 2) UPDATE: 
It will update the data in the database.
```Syntax for UPDATE ```
```
UPDATE table_name 
SET column_name = value 
WHERE condition;  
```

###### 3) DELETE:
It will delete the data from the database.
```Syntax for DELETE```
```
DELETE FROM table_name 
WHERE condition;  
```

###### 4) CREATE TABLE:
It will create a new table.
```Syntax for DELETE```
```
create table "tablename"  
("column1" "data type",  
"column2" "data type",  
"column3" "data type",    
"column4" "data type");  
```
###### 5) ALTER TABLE: 
It is used to modify the existing table.
```Syntax for ALTER TABLE```
```
ALTER TABLE table_name 
MODIFY column_name column_type;  
```
###### 6) DROP TABLE: 
It will delete the table from the database.
```Syntax for DROP TABLE```
```
DROP TABLE "table_name";
```

###### 7) DROP DATABASE: 
It will delete the database.
```Syntax for DROP TABLE```
```
DROP DATABASE database_name;  
```

###### 8) CREATE DATABASE: 
It will create a new database.
```Syntax for CREATE DATABASE```
```
CREATE DATABASE database_name;  
```

###### 9) RENAME DATABASE: 
It is used to modify a database.
```Syntax for RENAME DATABASE```
```
RENAME DATABASE old_db_name TO new_db_name;  
```
###### 10) INSERT INTO: 
It inserts new data into a database.
```Syntax for INSERT INTO```
```
INSERT INTO table_name  
VALUES (value1, value2, value3....);  
```

### What is JOIN?
- JOIN means to combine something. In the case of SQL, JOIN means "to combine two or more tables".
- The SQL JOIN clause takes records from two or more tables in a database and combines them together.

### Types of JOINS:
- Inner join,
- Outer join
    -	A)Left outer join,
	-   B)Right outer join, and
	-   C)Full outer join.
- Cross join.


##### 1) Inner join:
- The joining of two or more tables based on a common field between them is called inner join.
- SQL INNER JOIN also known as the simple join is the most common type of join.
```Syntax for inner join:```
```
SELECT *   
FROM table1, table2  
WHERE table1.column_name = table2.column_name;  
```
##### 2) Outer join:
###### A) Left outer join:
The SQL left join returns all the values from the left table and it also includes matching values from the right table, if there are no matching join values it returns NULL.
```Syntax for left outer join```
```SELECT table1.column1, table2.column2   
FROM table1   
LEFTJOIN table2  
ON table1.column_field = table2.column_field;  
```

###### B) Right outer join:
The SQL right join returns all the values from the rows of the right table. It also includes the matched values from the left table but if there is no matching in both tables, it returns NULL.
```Syntax for right outer join:```
```
SELECT table1.column1, table2.column2  
FROM table1   
RIGHT JOIN table2  
ON table1.column_field = table2.column_field;  
```

###### c) Full outer join:
The SQL full join is the result of a combination of both left and right outer join and it joins tables have all the records from both tables. It puts NULL on the place of matches not found.
```Syntax for full outer join:```
```
SELECT *  
FROM table1  
FULL OUTER JOIN table2  
ON table1.column_name = table2.column_name;  
```

##### 3) Cross join:
When each row of the first table is combined with each row from the second table, known as Cartesian join or cross join. In general words, we can say that SQL CROSS JOIN returns the Cartesian product of the sets of rows from the joined table.
```Syntax for cross join:```
```
SELECT * FROM TABLE1 CROSS JOIN TABLE2;  
OR  
SELECT * FROM  TABLE1 , TABLE2;  
```


### Aggregate functions in SQL:
 In database management, an aggregate function is a function where the values of multiple rows are grouped together as input on certain criteria to form a single value of more significant meaning.

#### Various Aggregate Functions are:
```
1) Count()
2) Sum()
3) Avg()
4) Min()
5) Max()
```
```Sample data:```
| Id | Name | Salary |
| -- | ---- | ------ |
| 1 | A | 80 |
| 2 | B | 40 |
| 3 | C | 60 |
| 4 | D | 70 |
| 5 | E | 60 |
| 6 | F | Null |

##### 1) count function:
It counts the number of rows from the table and returns a single value.
```Examples:```
```
Count(*): Returns total number of records .i.e 6.
Count(salary): Return number of Non-Null values over the column salary. i.e 5.
Count(Distinct Salary):  Return number of distinct Non-Null values over the column salary .i.e 4
```
##### 2) Sum():
It will return the sum value of a row as a single value.
```Examples:```
```
sum(salary):  Sum all Non-Null values of Column salary i.e., 310.
sum(Distinct salary): Sum of all distinct Non-Null values i.e., 250.
```

##### 3) Avg():
It will return the average of a row as a single value. 
```Examples:```
```
Avg(salary) = Sum(salary) / count(salary) = 310/5
Avg(Distinct salary) = sum(Distinct salary) / Count(Distinct Salary) = 250/4
```
 

##### 4) Min():
It will return the minimum value of the given row.
```Examples:```
```
Min(salary): Minimum value in the salary column except NULL i.e., 40.
```

##### 5) Max():
It will return the maximum value of the given row.
```Examples:```
```
Max(salary): Maximum value in the salary i.e., 80.
```

# Links
| Topics |  Related link |
| ------ | ---- |
| Basics | https://www.javatpoint.com/sql-tutorial |
| Joins | https://www.javatpoint.com/sql-join |
| Aggregate | https://www.javatpoint.com/dbms-sql-aggregate-function |



